import sys
from pirc522 import RFID


class SensorFlow(object):
    def get_flow(self):
        self.callback()
        return self.flow

    def __init__(self):
        self.flow = 0

    def callback(self):
        self.flow = self.flow + 1


class SensorNFC(object):
    def __init__(self):
        self.rdr = RFID()

    def esperaNFC(self):
        self.rdr.wait_for_tag()

    def esperaNoNFC(self):
        error = False
        data = -1
        while error is not True or data is not None:
            (error, data) = self.rdr.request()
            print error, data
            time.sleep(0.2)


class CommsMQTT():
    pass


class MyGarden():
    def __init__(self):
        self.sensornfc = SensorNFC()
        self.sensorflow = SensorFlow()

    def run(self):
        print "Inici"
        while True:
            self.sensornfc.esperaNFC()
            print "NFC detectat"
            flow1 = self.sensorflow.get_flow()
            self.sensornfc.esperaNoNFC()
            flow2 = self.sensorflow.get_flow()
            print "NFC No detectat", flow2-flow1


if __name__ == "__main__":
    mygarden = MyGarden(sys.args)
    mygarden.run()
